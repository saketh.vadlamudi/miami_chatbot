import re
import nltk
import datetime
from nltk.tag.stanford import StanfordNERTagger
from nltk.tokenize import word_tokenize
#from nltk.tag.corenlp import CoreNLPNERTagger
import requests
import data_miami

debug=1



st7 = StanfordNERTagger('stanford-ner-2017-06-09/classifiers/english.muc.7class.distsim.crf.ser.gz', 'stanford-ner-2017-06-09/stanford-ner.jar',encoding='utf-8')
st3 = StanfordNERTagger('stanford-ner-2017-06-09/classifiers/english.all.3class.distsim.crf.ser.gz', 'stanford-ner-2017-06-09/stanford-ner.jar',encoding='utf-8')



#should return one of the five intents getEmail,getDocument,addCall,addMeeting,addAction
def get_intent(sentence):
	pattern_email=re.compile("e?-?mails?",re.IGNORECASE)
	result=pattern_email.search(sentence)
	if(result):
		return "getEmail"
	pattern_document=re.compile("(\w+)\.(txt|py)")
	result=pattern_document.search(sentence)
	if(result):
		return "getDocument"
        pattern_call=re.compile("call\W")
        result=pattern_call.search(sentence)
        if(result):
                return "addCall"
        pattern_meeting=re.compile("meeting\W")
        result=pattern_meeting.search(sentence)
        if(result):
                return "addMeeting"
	else:
		return "addAction"


def process_getEmail(sentence):
	list_persons=[]
	date_details=[]
	time_st=""

	month_dict={"jan":1,"feb":2,"mar":3,"apr":4,"may":5,"jun":6,"jul":7,"aug":8,"sep":9,"oct":10,"nov":11,"dec":12}
	month_dict2={"january":1,"february":2,"march":3,"april":4,"may":5,"june":6,"july":7,"august":8,"september":9,"october":10,"november":11,"december":12}

	tokens = nltk.word_tokenize(sentence)
	tags = st3.tag(tokens)
	if debug:
		print "st3 tags in getEmail intent are ",tags
	for tag in tags:
        	if tag[1]=='PERSON':
			list_persons.append(tag[0].lower())
	print "email person details are ",list_persons
	if(len(list_persons)!=2):
		print "check the names entered and enter the names of persons with first letter capital"
		return 
	tags_st7=st7.tag(tokens)
	if(debug):
		print "st7 tags in getEmail intent are ",tags_st7
        for tag in tags_st7:
                if tag[1]=='DATE':
                        date_details.append(tag[0].lower())
	print "email date details are ",date_details
	#get the time stamp object from date_details
	try:
		time_st=datetime.datetime.now()-datetime.timedelta(int(date_details[1]))
		if(debug):
			print "time stamp from utterance is ",time_st
	except ValueError:
		if(debug):
			print "mail not in the format of last n days"
		if(date_details[1].lower() in month_dict):
			current_year=datetime.datetime.now().year
			time_st=datetime.datetime(current_year,month_dict[date_details[1].lower()],1,0,0)
		elif(date_details[1].lower() in month_dict2):
                        current_year=datetime.datetime.now().year
                        time_st=datetime.datetime(current_year,month_dict2[date_details[1].lower()],1,0,0)
	except Exception as Ex:
		print "check number of days",type(Ex),Ex 

	result_list=data_miami.get_mails(list_persons[0],list_persons[1],time_st)
	#print "length of mail lsit is ",len(result_list)
	if(len(result_list)==0):
		print "There are no mails for specified persons at specified time.please check email data."
	for result in result_list:
		print result,"\n"

def process_getDocument(sentence):
	document_details=[]
        tokens = nltk.word_tokenize(sentence)
        tags = nltk.pos_tag(tokens)
        print tags
	for tag in tags:
                if tag[1]=='NN' or tag[1]=='NNP' or tag[1]=='NNS':
                        document_details.append(tag[0].lower())
        #print list_persons[0],list_persons[1]
	if(len(document_details)!=2):
		print "Enter names of the document in docname.file_type format"
		return 
        data_miami.get_document(document_details[0],document_details[1])


def process_addCall(sentence):
	sentence=sentence.replace('to','')
        call_details=[]
        tokens = nltk.word_tokenize(sentence)
        tags = nltk.pos_tag(tokens)
	if(debug):
        	print tags
        for tag in tags:
                if tag[1]=='NNP' or tag[1]=='NNS':
                        call_details.append(tag[0].lower())
        if(debug):
		print "the name of person in call is ",call_details[0]
        if(len(call_details)==0):
                print "Desired contact is not in the database.please add his/her details"
                return
        result_list=data_miami.add_call(call_details[0])
	if(len(result_list)==0):
                print "Desired contact is not in the database.please add his/her details"
                return
	for result in result_list:
                print result,"\n"

def process_addMeeting(sentence):
	sentence=sentence.replace('and','')
	sentence=sentence.replace('today','Today')
        person_details=[]
	date_details=[]
	time_details=[]
        tokens = nltk.word_tokenize(sentence)
        tags_st3 = st3.tag(tokens)
        if(debug):
		print "st3 tags in addMeeting intent are ",tags_st3
	#print "printing stanford tags"
	tags_st7=st7.tag(tokens)
	if(debug):
		print "st7 tags in addMeeting intent are ",tags_st7

	for tag in tags_st3:
		if tag[1]=='PERSON':
                        person_details.append(tag[0].lower())	

        for tag in tags_st7:
                #if tag[1]=='PERSON':
                	#person_details.append(tag[0].lower())
		if tag[1]=='DATE':
			date_details.append(tag[0].lower())
		elif tag[1]=='TIME':
			time_details.append(tag[0].lower())
        print "persons details",person_details,"\n"
	print "date details",date_details,"\n"
	print "time details",time_details,"\n"

        if(len(person_details)==0):
                print "Enter name of the persons with first letter caps"
                return 1
        elif(len(date_details)==0):
                print "check the date format"
                return 1
        if(len(time_details)==0):
                print "check the time format"
                return 1
	else:
		return 0


def process_addAction(sentence):
	sentence=sentence.replace('today','Today')
	action_details=[]
        date_details=[]
        time_details=[]
        tokens = nltk.word_tokenize(sentence)
        tags_nltk = nltk.pos_tag(tokens)
	if(debug):
        	print "nltk tags in addAction intent are ",tags_nltk
	flag_action=0
	for tag in tags_nltk:
                if tag[1]=='IN':
                        flag_action=0
                        break
		if(flag_action):
                        action_details.append(tag[0])
		if tag[1]=='TO':
			flag_action=1

		

        #print "printing stanford tags"
        tags_st7=st7.tag(tokens)
        if(debug):
		print "st7 tags in addAction inetnt ate ",tags_st7
        for tag in tags_st7:
                #if tag[1]=='PERSON':
                        #action_details.append(tag[0].lower())
                if tag[1]=='DATE':
                        date_details.append(tag[0].lower())
                elif tag[1]=='TIME':
                        time_details.append(tag[0].lower())
        print "action details",action_details,"\n"
        print "date details",date_details,"\n"
        print "time details",time_details,"\n"

        if(len(action_details)==0):
                print "check action details"
                return 1
        elif(len(date_details)==0):
                print "check the date format"
                return 1
        if(len(time_details)==0):
                print "check the time format"
                return 1
        else:
                return 0








		

if __name__=="__main__":

	sentence="Retrieve Email from John to Mary in the last 4 days"
	sentence2="get document README.txt from /Users/sakethv/Desktop/miami"
	sentence3="call saketh now"
	sentence4="schedule meeting between Peter John Sam regarding project_x  on 8th Oct 2017 at 3:00 pm"
	sentence5="remind me to attend conference on 12th Aug 2018 at 6:00 pm"
	

	sentence11="retrieve e-mail from John to Nava in the last 5 days" #try with a name not in database
	sentence12="get all the mails between John and Mary in  the last 4 days" #try with diffrent utterance of mail format
	sentence13="retrieve e-mail from John to Mary after 1st oct 2017"


	sentence21="get document sak.txt from /Users/sakethv/Desktop/miami" #try with non existing document
	
	sentence31="make a call to Saketh now"
	sentence32="call to Saketh now"

	sentence41="schedule meeting between Peter John Sam regarding project_x today"
	sentence42="arrange a meeting between Saketh and Nava after 3 days"
	sentence43="arrange a meeting between Saketh and Nava tomorrow"


	sentence="remind me to attend conference today at 6:00 pm"
	sentence52="remind me to go for goa trip today"
	
	intent=get_intent(sentence)
	print intent
	if(intent=="getEmail"):
		process_getEmail(sentence)
	elif(intent=="getDocument"):
			process_getDocument(sentence)
	elif(intent=="addCall"):
			process_addCall(sentence)
	elif(intent=="addMeeting"):
			process_addMeeting(sentence)
	elif(intent=="addAction"):
			process_addAction(sentence)
	






