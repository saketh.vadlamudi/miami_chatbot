import sqlite3
import datetime

conn = sqlite3.connect('databse_miami.db', detect_types=sqlite3.PARSE_DECLTYPES)
c = conn.cursor()

#data for "mail" table,i..e from,to,time_stamp,mail_content
mail_list=[('john','mary',datetime.datetime.now(),'this is first mail'),
	('john','mary',datetime.datetime(2017,10,1,18,30),'this is first mail'),
	('mary','john',datetime.datetime.now(),'this is second mail'),
	('saketh','nava',datetime.datetime.now(),'this is third mail'),
	('nava','saketh',datetime.datetime.now(),'this is fourth mail')]


def create_mail_data():
	c.execute('''drop table if exists mail''')
	c.execute('''CREATE TABLE mail(from_person text,to_person text,time_stamp timestamp,mail_content text)''')
	c.executemany('''INSERT INTO mail VALUES (?,?,?,?)''',mail_list)
	conn.commit()
	#conn.close()

def get_mails(s1,s2,time_st):
	result_list=[]
	for row in c.execute("SELECT * FROM mail WHERE from_person = ? and to_person=? and time_stamp>?",(s1,s2,time_st) ):
		#print row,"\n"
		result_list.append(row)
	return result_list

#data for document table,i..e document_name,path_of_document
document_list=[("README.txt","/Users/sakethv/Desktop/miami"),
	("example.py","/Users/sakethv/Desktop/miami"),
	("interview.txt","/Users/sakethv/Desktop/miami"),
	("solution.txt","/Users/sakethv/Desktop/miami")]



def create_document_data():
        c.execute('''drop table if exists document''')
        c.execute('''CREATE TABLE document(document_name text,document_path text)''')
        c.executemany('''INSERT INTO document VALUES (?,?)''',document_list)
        conn.commit()
        #conn.close()

def get_document(document_name,document_path):
	try:
		with open(document_path+"/"+document_name,'r') as fin:
			print fin.read()
	except:
		print "Required document is not present at the mentioned location"
		
#data for call table,i..e contact_name,contact_number
call_list=[("saketh","9892275485"),
        ("miami","123456")]



def create_call_data():
        c.execute('''drop table if exists call''')
        c.execute('''CREATE TABLE call(contact_name text,contact_number text)''')
        c.executemany('''INSERT INTO call VALUES (?,?)''',call_list)
        conn.commit()
        #conn.close()

def add_call(contact_namee):
        try:
		result_list=[]
        	for row in c.execute("SELECT * FROM call WHERE contact_name= ? ", (contact_namee,) ):
                	#print row,"\n"
                	result_list.append(row)
        	return result_list


        except Exception as ex:
                print "Required contact is not present in the database",ex



if __name__=="__main__":
	create_mail_data()
	#get_mails("john","mary",datetime.datetime.now()-datetime.timedelta(4))
	create_document_data()
	#get_document("README.txt","/Users/sakethv/Desktop/miami")
	create_call_data()
	add_call("saketh")
	print "data created successfully"




