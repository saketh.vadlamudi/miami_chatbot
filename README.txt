1) install NLTK by following command
sudo pip install -U nltk

2)install numpy package by following command
sudo pip install -U numpy

3)download dependant packages from nltk

import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('tagsets')
nltk.downlaod('stopwords')


4)Download folder "stanford-ner-2017-06-09" from the link https://nlp.stanford.edu/software/CRF-NER.html#Download

5)scripts and their purpose

	a)data_miami.py----->script to create mock data and modules related to query the data
	b)demo.py----------->main script to demo intention detection and fonding slot entities
		Note: order of execution of sripts is 
		1)python data_miami.py (first run this script to create mock data)
		2)python demo.py
	c)README.txt------>instructions to get dependencies and run demo
	d)interview.txt---->problem statement
	e)solution.txt----->soultion approach description
	
